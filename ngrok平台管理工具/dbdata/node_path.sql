-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2021-06-10 16:06:53
-- 服务器版本： 5.7.26
-- PHP 版本： 7.3.4

 

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `ngrok_manager`
--

-- --------------------------------------------------------

--
-- 表的结构 `node_path`
--

CREATE TABLE `node_path` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键',
  `created_at` datetime(3) DEFAULT NULL COMMENT '''创建时间''',
  `updated_at` datetime(3) DEFAULT NULL COMMENT '''更新时间''',
  `deleted_at` datetime(3) DEFAULT NULL COMMENT '''删除时间''',
  `sort` bigint(20) DEFAULT '999',
  `pid` bigint(20) UNSIGNED DEFAULT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` char(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `component` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `iview` json DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `always_show` tinyint(1) DEFAULT '1',
  `hidden` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `node_path`
--

INSERT INTO `node_path` (`id`, `created_at`, `updated_at`, `deleted_at`, `sort`, `pid`, `name`, `description`, `url`, `path`, `component`, `redirect`, `iview`, `status`, `always_show`, `hidden`) VALUES
(31, '2021-04-07 18:45:11.570', '2021-04-09 14:53:41.502', NULL, 999, 0, '系统权限', 'Syspermission', '#', '/syspermission', 'layout/Layout', 'noRedirect', '{\"icon\": \"el-icon-s-custom\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}], \"title\": \"sysPermission\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1),
(32, '2021-04-07 18:45:58.971', '2021-04-09 14:55:16.921', NULL, 999, 31, '用户管理', 'User', '/user/index', 'user', 'layout/Layout', '', '{\"icon\": \"el-icon-user-solid\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}], \"title\": \"userManager\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1),
(33, '2021-04-07 18:46:46.334', '2021-06-08 11:26:43.991', NULL, 999, 31, '角色管理', 'rolesManager', '/roles/index', 'roles', 'layout/Layout', '', '{\"icon\": \"el-icon-user\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}], \"title\": \"rolesManager\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1),
(34, '2021-04-07 18:47:35.887', '2021-05-27 19:33:12.672', NULL, 999, 31, '页面权限', 'pageNodePermission', '/pagenode/index', 'pagenode', 'layout/Layout', '', '{\"icon\": \"el-icon-link\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}], \"title\": \"pageNodePermission\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1),
(35, '2021-04-07 19:29:47.221', '2021-05-27 19:32:39.406', NULL, 999, 0, '系统配置', 'SystemConfig', '#', '/system', 'layout/Layout', 'noRedirect', '{\"icon\": \"el-icon-s-tools\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}], \"title\": \"SystemConfig\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1),
(36, '2021-04-07 19:30:25.812', '2021-04-13 15:09:21.296', NULL, 999, 35, '系统配置', 'Configs', '/system/config', 'configs', 'layout/Layout', '', '{\"icon\": \"el-icon-setting\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}, {\"name\": \"yanshi\"}], \"title\": \"SystemConfig\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1),
(46, '2021-04-07 19:44:45.770', '2021-04-13 15:10:32.332', NULL, 999, 0, '用户操作', 'Operation', '#', '/operation', 'layout/Layout', 'noRedirect', '{\"icon\": \"el-icon-thumb\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}, {\"name\": \"yanshi\"}], \"title\": \"operating\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1),
(47, '2021-04-07 19:45:38.475', '2021-04-13 16:00:56.520', NULL, 999, 46, '操作路径', 'operatingpath', '/operation/operatingpath', 'operatingpath', 'layout/Layout', '', '{\"icon\": \"el-icon-connection\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}], \"title\": \"operatingpath\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1),
(48, '2021-04-07 19:46:08.232', '2021-04-13 15:10:51.775', NULL, 999, 46, '操作日志', 'operationlog', '/operation/operationlog', 'operationlog', 'layout/Layout', '', '{\"icon\": \"el-icon-document-copy\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}, {\"name\": \"yanshi\"}], \"title\": \"operationlog\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1),
(59, '2021-06-09 15:58:10.846', '2021-06-09 15:58:10.846', NULL, 999, 0, '反向代理管理', 'ngrok', '#', '/ngrok', 'layout/Layout', 'noRedirect', '{\"icon\": \"el-icon-s-promotion\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}], \"title\": \"反向代理管理\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1),
(60, '2021-06-09 15:59:29.348', '2021-06-09 16:47:19.035', NULL, 1001, 59, '反向代理用户管理', '', '/ngrok/user', '/ngrok/user', 'layout/Layout', '', '{\"icon\": \"el-icon-user-solid\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}], \"title\": \"反代用户\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1),
(61, '2021-06-09 16:00:37.858', '2021-06-09 16:47:36.865', NULL, 999, 59, '反向代理日志管理', 'ngrok/log', '/ngrok/log', '/ngrok/log', 'layout/Layout', '', '{\"icon\": \"el-icon-s-order\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}], \"title\": \"反代日志\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1),
(62, '2021-06-09 16:05:28.148', '2021-06-09 16:47:31.931', NULL, 1000, 59, '反向代理流量管理', 'networkflow', '/ngrok/networkflow', '/ngrok/networkflow', 'layout/Layout', '', '{\"icon\": \"el-icon-sort\", \"affix\": false, \"roles\": [{\"name\": \"super_admin\"}], \"title\": \"反代流量\", \"noCache\": false, \"breadcrumb\": true}', 1, 1, 1);

--
-- 转储表的索引
--

--
-- 表的索引 `node_path`
--
ALTER TABLE `node_path`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_node_path_redirect` (`redirect`),
  ADD KEY `idx_node_path_deleted_at` (`deleted_at`),
  ADD KEY `idx_node_path_url` (`url`),
  ADD KEY `idx_node_path_path` (`path`),
  ADD KEY `idx_node_path_component` (`component`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `node_path`
--
ALTER TABLE `node_path`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=63;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
