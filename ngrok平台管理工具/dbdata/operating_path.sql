-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2021-06-10 16:07:29
-- 服务器版本： 5.7.26
-- PHP 版本： 7.3.4
 

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `ngrok_manager`
--

-- --------------------------------------------------------

--
-- 表的结构 `operating_path`
--

CREATE TABLE `operating_path` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键',
  `created_at` datetime(3) DEFAULT NULL COMMENT '''创建时间''',
  `updated_at` datetime(3) DEFAULT NULL COMMENT '''更新时间''',
  `deleted_at` datetime(3) DEFAULT NULL COMMENT '''删除时间''',
  `path` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client` enum('APP','WEB','通用') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '''客户端''',
  `operating_name` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isrecord` tinyint(1) DEFAULT '1' COMMENT '是否记录'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `operating_path`
--

INSERT INTO `operating_path` (`id`, `created_at`, `updated_at`, `deleted_at`, `path`, `client`, `operating_name`, `isrecord`) VALUES
(6, '2021-03-03 11:58:48.000', '2021-03-03 11:58:51.000', NULL, '/admin/user/logout', '通用', '退出登录', 1),
(8, '2021-03-03 11:59:57.000', '2021-03-11 13:51:01.308', NULL, '/api/apiv1/getdata', '通用', '获取设备数据', 0),
(9, '2021-03-03 12:00:43.000', '2021-03-03 12:00:46.000', NULL, '/admin/user/myinfo', '通用', '获取用户信息', 1),
(10, '2021-03-03 12:02:03.000', '2021-03-03 12:02:05.000', NULL, '/admin/changepwd', '通用', '修改用户密码', 1),
(11, '2021-03-03 12:02:49.000', '2021-03-03 12:02:51.000', NULL, '/admin/useredit', '通用', '修改用户信息', 1),
(12, '2021-03-03 19:34:24.000', '2021-03-08 14:32:44.909', NULL, '/admin/login', '通用', '登录', 0),
(17, '2021-03-05 15:49:37.310', '2021-03-05 15:49:37.310', NULL, '/admin/operating', '通用', '用户操作', 1),
(18, '2021-03-05 15:52:37.288', '2021-03-05 16:03:58.128', NULL, '/admin/syspermission', 'WEB', '系统权限', 1),
(23, '2021-03-06 17:23:59.783', '2021-03-06 17:23:59.783', NULL, '/admin/operating/createOperatingPath', 'WEB', '创建操作路径', 1),
(25, '2021-03-06 17:27:11.484', '2021-03-06 17:27:20.810', NULL, '/admin/operating/updateOperatingPath', 'WEB', '修改操作路径', 1),
(26, '2021-03-06 17:27:51.488', '2021-03-06 17:27:51.488', NULL, '/admin/operating/delOperatingPath', 'WEB', '删除操作路径', 1),
(32, '2021-03-06 19:38:40.000', '2021-03-06 19:32:57.822', NULL, '/admin/operating/getPath', 'WEB', '获取所有操作路径', 0),
(33, '2021-03-06 19:38:36.000', '2021-03-06 19:38:44.000', NULL, '/admin/operatinglog/getlog', 'WEB', '获取全部操作日志', 0),
(34, '2021-03-06 19:38:32.000', '2021-03-08 10:40:44.924', NULL, '/admin/operating/getPathId', 'WEB', '获取单个操作路径', 0),
(44, NULL, NULL, NULL, '/admin/system/config', 'WEB', '获取系统配置', 1),
(45, NULL, NULL, NULL, '/admin/system/setconfig', 'WEB', '修改系统配置', 1),
(46, NULL, NULL, NULL, '/admin/users', 'WEB', '获取所有用户', 1),
(47, NULL, NULL, NULL, '/admin/syspermission/roles/names', 'WEB', '获取所有角色', 1),
(50, NULL, NULL, NULL, '/admin/user', '通用', '获取单个用户', 1),
(59, '2021-04-13 16:05:01.938', '2021-04-13 16:05:01.938', NULL, '/admin/syspermission/pagenode/index', 'WEB', '页面权限列表', 1),
(60, '2021-04-13 16:05:01.938', '2021-04-13 16:05:01.938', NULL, '/admin/syspermission/roles/index', 'WEB', '获取用户列表', 1),
(61, '2021-06-10 16:04:46.912', '2021-06-10 16:04:46.912', NULL, '/applications/ngrok/v1/users/getall', '通用', '获取反向代理用户列表', 0),
(62, '2021-06-10 16:05:18.005', '2021-06-10 16:05:18.005', NULL, '/applications/ngrok/v1/users/adduser', '通用', '添加反向代理用户', 1),
(63, '2021-06-10 16:05:32.154', '2021-06-10 16:05:32.154', NULL, '/applications/ngrok/v1/users/deluser', '通用', '删除反向代理用户', 1),
(64, '2021-06-10 16:05:51.031', '2021-06-10 16:05:51.031', NULL, '/applications/ngrok/v1/users/updatestatusbyid', '通用', '改变方向代理用户状态', 1),
(65, '2021-06-10 16:06:08.467', '2021-06-10 16:06:08.467', NULL, '/applications/ngrok/v1/users/updateuserbyid', '通用', '修改反向代理用户数据', 1),
(66, '2021-06-10 16:06:26.260', '2021-06-10 16:06:26.260', NULL, '/applications/ngrok/v1/users/getuserbyid', '通用', '获取单个反向代理用户', 0);

--
-- 转储表的索引
--

--
-- 表的索引 `operating_path`
--
ALTER TABLE `operating_path`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_operating_path_deleted_at` (`deleted_at`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `operating_path`
--
ALTER TABLE `operating_path`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=67;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
