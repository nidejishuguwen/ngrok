package model

import (
	"time"

	"github.com/Unknwon/goconfig"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var db *gorm.DB

func InitMysql(configpath string) {
	var user User
	var err error

	// 参考 https://github.com/go-sql-driver/mysql#dsn-data-source-name 获取详情
	if configpath == "" {
		configpath = "ngrokuser.cfg"
	}
	cfg, err := goconfig.LoadConfigFile(configpath)

	if err != nil {
		panic("未发现ngrokuser.cfg文件，请在exe文件同级目录创建ngrokuser.cfg文件")
	}
	mysqlUname, err := cfg.GetValue("mysql", "username")
	if err != nil {
		panic("请正确配置数据库")
	}
	mysqlPwd, err := cfg.GetValue("mysql", "password")
	if err != nil {
		panic("请正确配置数据库")
	}
	mysqlUrl, err := cfg.GetValue("mysql", "url")
	if err != nil {
		panic("请正确配置数据库")
	}
	mysqlTablename, err := cfg.GetValue("mysql", "tablename")
	if err != nil {
		panic("请正确配置数据库")
	}
	dsn := mysqlUname + ":" + mysqlPwd + "@tcp(" + mysqlUrl + ")/" + mysqlTablename + "?charset=utf8mb4&parseTime=True&loc=Local"
	//fmt.Println("dsn", dsn)
	db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	db.Logger.LogMode(0)
	if err != nil {
		panic("请正确配置数据库")
	}
	sqlDB, err := db.DB()
	if err != nil {
		panic("请正确配置数据库")
	}
	sqlDB.SetMaxOpenConns(100)
	sqlDB.SetConnMaxLifetime(time.Hour)
	db.AutoMigrate(&User{})

	db.First(&user)
	//fmt.Println(err)
}
