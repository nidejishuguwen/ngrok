package model

import (
	"errors"
	"fmt"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Subdomain string `gorm:"comment:域名;type:char(64);not null"`
	Username  string `gorm:"comment:用户名;type:char(64);not null"`
	Password  string `gorm:"comment:密码;type:char(64);not null"`
}

//测试
func (m *User) Test() {
	err := m.AddUser("fuxing", "fuxingxiongdi123")
	if err != nil {
		//fmt.Println(err)
	}
	_, err = m.CheckUser("fuxing", "fuxingxiongdi123")
	if err != nil {
		//fmt.Println(err)
	}
}

//添加用户
func (m *User) AddUser(username string, password string) (rterr error) {
	// //fmt.Println("====模拟注册====")
	user := User{}
	user.Username = username
	user.Subdomain = username
	user.Password = password //模拟注册是传递的密码
	err := db.Where("username = ?", username).First(&user).Error

	if err == nil {
		return errors.New("用户已存在")
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost) //加密处理
	if err != nil {
		//fmt.Println(err)
		return errors.New("密码无法加密")
	}
	user.Password = string(hash)
	//fmt.Println(user.Password)
	rterr = db.Create(&user).Error
	if rterr == nil {
		fmt.Printf("成功添加用户 %s\r\n", username)
	}
	return rterr
}

//重置用户密码
func (m *User) ResetUserPwd(username string) (rterr error) {
	// //fmt.Println("====模拟注册====")
	user := User{}
	err := db.Where("username = ?", username).First(&user).Error
	if err != nil {
		return errors.New("用户不存在")
	}
	hash, err := bcrypt.GenerateFromPassword([]byte("abc123456"), bcrypt.DefaultCost) //加密处理
	if err != nil {
		//fmt.Println(err)
		return errors.New("密码无法加密")
	}
	user.Password = string(hash)
	rterr = db.Save(&user).Error
	if rterr == nil {
		fmt.Println("密码重置为abc123456")
	}
	return rterr
}

//更新密码
func (m *User) UpdateUser(username string, password string, passwordnew string) (rterr error) {
	user, err := m.CheckUser(username, password)
	if err != nil {
		return err
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(passwordnew), bcrypt.DefaultCost) //加密处理
	if err != nil {
		return errors.New("密码无法加密")
	}
	user.Password = string(hash)
	rterr = db.Save(&user).Error
	if rterr == nil {
		fmt.Printf("密码更改为 %s\r\n", passwordnew)
	}
	return rterr
}

//更新域名
func (m *User) UpdateSubdomain(username string, password string, subdomain string) (rterr error) {
	user, err := m.CheckUser(username, password)
	if err != nil {
		return err
	}
	if err != nil {
		//fmt.Println(err)
		return errors.New("密码无法加密")
	}
	user.Subdomain = subdomain
	rterr = db.Save(&user).Error
	if rterr == nil {
		fmt.Printf("域名更新为 %s\r\n", user.Subdomain)
	}
	return
}

//检查用户名密码
func (m *User) CheckUser(username string, password string) (user User, rterr error) {
	// //fmt.Println("====模拟登录====")
	user = User{}
	err := db.Where("username = ?", username).First(&user).Error

	if err != nil {
		rterr = errors.New("账号或密码错误")
		return
	}
	// 密码验证
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)) //验证（对比）
	if err != nil {
		rterr = errors.New("账号或密码错误")
		return
	}
	rterr = nil
	fmt.Printf("\r\n用户名密码正确,域名%s\r\n", user.Subdomain)
	return
}
