module ngrok

go 1.13

require (
	github.com/Unknwon/goconfig v1.0.0
	github.com/alecthomas/log4go v0.0.0-20180109082532-d146e6b86faa
	github.com/go-sql-driver/mysql v1.7.0 // indirect
	github.com/gorilla/websocket v1.1.0
	github.com/inconshreveable/go-vhost v1.0.0
	github.com/inconshreveable/mousetrap v1.1.0
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0 // indirect
	github.com/kr/binarydist v0.1.0 // indirect
	github.com/nsf/termbox-go v1.1.1
	github.com/rcrowley/go-metrics v0.0.0-20201227073835-cf1acfcdf475
	github.com/smartystreets/goconvey v1.8.1 // indirect
	golang.org/x/crypto v0.21.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/inconshreveable/go-update.v0 v0.0.0-20150814200126-d8b0b1d421aa
	gopkg.in/yaml.v1 v1.0.0-20140924161607-9f9df34309c0
	gorm.io/driver/mysql v0.3.2
	gorm.io/gorm v0.2.38
)
